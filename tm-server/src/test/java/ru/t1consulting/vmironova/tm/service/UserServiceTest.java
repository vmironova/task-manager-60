package ru.t1consulting.vmironova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1consulting.vmironova.tm.api.service.dto.IUserDTOService;
import ru.t1consulting.vmironova.tm.configuration.ServerConfiguration;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.vmironova.tm.exception.field.IdEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.LoginEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.PasswordEmptyException;
import ru.t1consulting.vmironova.tm.exception.user.ExistsEmailException;
import ru.t1consulting.vmironova.tm.exception.user.ExistsLoginException;
import ru.t1consulting.vmironova.tm.exception.user.RoleEmptyException;
import ru.t1consulting.vmironova.tm.marker.UnitCategory;

import static ru.t1consulting.vmironova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @Nullable
    private static IUserDTOService SERVICE;

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        SERVICE = context.getBean(IUserDTOService.class);
    }

    @Before
    public void before() throws Exception {
        SERVICE.add(USER_TEST);
    }

    @After
    public void after() throws Exception {
        @Nullable UserDTO user = SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) SERVICE.remove(user);
        user = SERVICE.findByLogin(ADMIN_TEST_LOGIN);
        if (user != null) SERVICE.remove(user);
    }

    @Test
    public void add() throws Exception {
        Assert.assertNotNull(SERVICE.add(ADMIN_TEST));
        @Nullable final UserDTO user = SERVICE.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(SERVICE.existsById(""));
        Assert.assertFalse(SERVICE.existsById(null));
        Assert.assertFalse(SERVICE.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(SERVICE.existsById(USER_TEST.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.findOneById(""));
        Assert.assertNull(SERVICE.findOneById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = SERVICE.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeById(""));
        SERVICE.add(ADMIN_TEST);
        Assert.assertNotNull(SERVICE.findOneById(ADMIN_TEST.getId()));
        SERVICE.removeById(ADMIN_TEST.getId());
        Assert.assertNull(SERVICE.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.create(null, ADMIN_TEST_PASSWORD));
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.create("", ADMIN_TEST_PASSWORD));
        Assert.assertThrows(ExistsLoginException.class, () -> SERVICE.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> SERVICE.create(ADMIN_TEST_LOGIN, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> SERVICE.create(ADMIN_TEST_LOGIN, ""));
        @NotNull final UserDTO user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        Assert.assertNotNull(user);
        @Nullable final UserDTO findUser = SERVICE.findOneById(user.getId());
        Assert.assertNotNull(findUser);
        Assert.assertEquals(user.getId(), findUser.getId());
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.create(null, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL));
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.create("", ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL));
        Assert.assertThrows(ExistsLoginException.class, () -> SERVICE.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> SERVICE.create(ADMIN_TEST_LOGIN, null, ADMIN_TEST_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> SERVICE.create(ADMIN_TEST_LOGIN, "", ADMIN_TEST_EMAIL));
        Assert.assertThrows(ExistsEmailException.class, () -> SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, USER_TEST_EMAIL));
        @NotNull final UserDTO user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        Assert.assertNotNull(user);
        @Nullable final UserDTO findUser = SERVICE.findOneById(user.getId());
        Assert.assertNotNull(findUser);
        Assert.assertEquals(user.getId(), findUser.getId());
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST_EMAIL, user.getEmail());
    }

    @Test
    public void createWithRole() throws Exception {
        @NotNull final Role role = Role.ADMIN;
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.create(null, ADMIN_TEST_PASSWORD, role));
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.create("", ADMIN_TEST_PASSWORD, role));
        Assert.assertThrows(ExistsLoginException.class, () -> SERVICE.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD, role));
        Assert.assertThrows(PasswordEmptyException.class, () -> SERVICE.create(ADMIN_TEST_LOGIN, null, role));
        Assert.assertThrows(PasswordEmptyException.class, () -> SERVICE.create(ADMIN_TEST_LOGIN, "", role));
        Assert.assertThrows(RoleEmptyException.class, () -> {
            @Nullable final Role nullRole = null;
            SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, nullRole);
        });
        @NotNull final UserDTO user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, Role.ADMIN);
        Assert.assertNotNull(user);
        @Nullable final UserDTO findUser = SERVICE.findOneById(user.getId());
        Assert.assertNotNull(findUser);
        Assert.assertEquals(user.getId(), findUser.getId());
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.findByLogin(""));
        @Nullable final UserDTO user = SERVICE.findByLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void remove() throws Exception {
        SERVICE.add(ADMIN_TEST);
        Assert.assertNotNull(SERVICE.findOneById(ADMIN_TEST.getId()));
        SERVICE.remove(ADMIN_TEST);
        Assert.assertNull(SERVICE.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void removeByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> SERVICE.removeByLogin(NON_EXISTING_USER_ID));
        SERVICE.add(ADMIN_TEST);
        SERVICE.removeByLogin(ADMIN_TEST_LOGIN);
        Assert.assertNull(SERVICE.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void setPassword() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.setPassword(null, ADMIN_TEST_PASSWORD));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.setPassword("", ADMIN_TEST_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> SERVICE.setPassword(USER_TEST.getId(), null));
        Assert.assertThrows(PasswordEmptyException.class, () -> SERVICE.setPassword(USER_TEST.getId(), ""));
        Assert.assertThrows(UserNotFoundException.class, () -> SERVICE.setPassword(NON_EXISTING_USER_ID, ADMIN_TEST_PASSWORD));
        SERVICE.setPassword(USER_TEST.getId(), ADMIN_TEST_PASSWORD);
        @Nullable final UserDTO user = SERVICE.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        SERVICE.setPassword(USER_TEST.getId(), USER_TEST_PASSWORD);
    }

    @Test
    public void updateUser() throws Exception {
        @NotNull final String firstName = "User_first_name";
        @NotNull final String lastName = "User_last_name";
        @NotNull final String middleName = "User_middle_name";
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.updateUser(null, firstName, lastName, middleName));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.updateUser("", firstName, lastName, middleName));
        SERVICE.updateUser(USER_TEST.getId(), firstName, lastName, middleName);
        @Nullable final UserDTO user = SERVICE.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test
    public void isLoginExists() throws Exception {
        Assert.assertFalse(SERVICE.isLoginExists(null));
        Assert.assertFalse(SERVICE.isLoginExists(""));
        Assert.assertTrue(SERVICE.isLoginExists(USER_TEST_LOGIN));
    }

    @Test
    public void isEmailExists() throws Exception {
        Assert.assertFalse(SERVICE.isEmailExists(null));
        Assert.assertFalse(SERVICE.isEmailExists(""));
        Assert.assertTrue(SERVICE.isEmailExists(USER_TEST_EMAIL));
    }

    @Test
    public void lockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.lockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> SERVICE.lockUserByLogin(NON_EXISTING_USER_ID));
        SERVICE.lockUserByLogin(USER_TEST_LOGIN);
        @Nullable final UserDTO user = SERVICE.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.unlockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> SERVICE.unlockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> SERVICE.unlockUserByLogin(NON_EXISTING_USER_ID));
        SERVICE.lockUserByLogin(USER_TEST_LOGIN);
        SERVICE.unlockUserByLogin(USER_TEST_LOGIN);
        @Nullable final UserDTO user = SERVICE.findByLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertFalse(user.getLocked());
    }

}
